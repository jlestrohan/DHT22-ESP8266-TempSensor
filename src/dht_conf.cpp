/* DHT CONF.cpp
 *   by Jack Lestrohan
 *
 * Created:
 *   13/02/2020 à 16:10:33
 * Last edited:
 *   13/02/2020 à 21:21:29
 * Auto updated?
 *   Yes
 *
 * Description:
 *   dht_conf.cpp main sensor handling file
**/
#include <Arduino.h>
#include <Ticker.h>
#include "dht_conf.h"

#define DHT22_PIN 13
dht DHT;

struct
{
    uint32_t total;
    uint32_t ok;
    uint32_t crc_error;
    uint32_t time_out;
    uint32_t connect;
    uint32_t ack_l;
    uint32_t ack_h;
    uint32_t unknown;
} stat = { 0,0,0,0,0,0,0,0};


/**
 * @brief  dewPoint function NOAA
 * @note   
 * @param  celsius: 
 * @param  humidity: 
 * @retval 
 */
double dewPoint(double celsius, double humidity)
{
  // (1) Saturation Vapor Pressure = ESGG(T)
  double RATIO = 373.15 / (273.15 + celsius);
  double RHS = -7.90298 * (RATIO - 1);
  RHS += 5.02808 * log10(RATIO);
  RHS += -1.3816e-7 * (pow(10, (11.344 * (1 - 1 / RATIO))) - 1);
  RHS += 8.1328e-3 * (pow(10, (-3.49149 * (RATIO - 1))) - 1);
  RHS += log10(1013.246);

  // factor -3 is to adjust units - Vapor Pressure SVP * humidity
  double VP = pow(10, RHS - 3) * humidity;

  // (2) DEWPOINT = F(Vapor Pressure)
  double T = log(VP / 0.61078); // temp var
  return (241.88 * T) / (17.558 - T);
}

/**
 * @brief  
 * @note   
 * @retval 
 */
DHTData readDHT()
{
  DHT.read22(DHT22_PIN);
  DHTData dataTmp;
  dataTmp.temperature = DHT.temperature;
  dataTmp.humidity = DHT.humidity;
  dataTmp.dewPoint = dewPoint(dataTmp.temperature, dataTmp.humidity);
  return dataTmp;
}

/**
 * @brief returns temperature as Celsius
 * @note   
 * @retval 
 */
/*float getDHTtemperature()
{
  return DHT.temperature();
}*/

/**
 * @brief returns temperature as Fahrenheit
 * @note   
 * @retval 
 */
/*float getDHThumidity()
{
  return DHT.humidity;
}*/

/**
 * @brief returns temperature as Fahrenheit  
 * @note   
 * @retval 
 */
/*float getDHTtemperatureF()
{
  return dht.readTemperature(true);
}*/

/**
 * @brief Compute heat index  
 * @note   
 * @retval 
 */
/*float getHeatIndex()
{
  float hi = dht.computeHeatIndex(getDHTtemperatureF(), getDHThumidity());
  float hiDegC = dht.convertFtoC(hi);
  return hiDegC;
}*/

/**
 * @brief returns the dew point  
 * @note   
 * @retval 
 */
/*double getDewPoint()
{
  return (dewPoint(getDHTtemperature(), getDHThumidity()));
}*/