/* MAIN.cpp
 *   by Jack Lestrohan
 *
 * Created:
 *   13/02/2020 à 02:51:53
 * Last edited:
 *   13/02/2020 à 21:33:13
 * Auto updated?
 *   Yes
 *
 * Description:
 *   Main DHT-22 Sensor program file
**/

// see https://www.carnetdumaker.net/articles/utiliser-un-capteur-de-temperature-et-dhumidite-dht11-dht22-avec-une-carte-arduino-genuino/

#include <Arduino.h>
#include <NTPClient.h>
#include <IotWebConf.h>
#include <MQTT.h>
#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson
#include <ArduinoOTA.h>
#include "uptime_formatter.h"
#include "RemoteDebug.h" //https://github.com/JoaoLopesF/RemoteDebug

#include "dht_conf.h"

#define NTP_OFFSET 60 * 60     // In seconds
#define NTP_INTERVAL 60 * 1000 // In miliseconds
#define NTP_ADDRESS "europe.pool.ntp.org"
// -- Configuration specific key. The value should be modified if config structure was changed.
#define CONFIG_VERSION "1000.10"
#define STRING_LEN 128

// interval in milliseconds between two readings of the sensor
#define UPDATE_INTERVAL 10000

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

RemoteDebug Debug;

// -- Initial name of the Thing. Used e.g. as SSID of the own Access Point.
const char thingName[] = "DHT22-ESP8266-TempSensor";

// -- Initial password to connect to the Thing, when it creates an own Access Point.
const char wifiInitialApPassword[] = "73727170";


// -- Status indicator pin (IotWebConf)
//      First it will light up (kept LOW), on Wifi connection it will blink,
//      when connected to the Wifi it will turn off (kept HIGH).
#define STATUS_PIN LED_BUILTIN

// -- When CONFIG_PIN is pulled to ground on startup, the Thing will use the initial
//      password to buld an AP. (E.g. in case of lost password)
#define CONFIG_PIN 5
#define SENSOR_LED 4

// -- Callback method declarations.
void wifiConnected();
void configSaved();
boolean formValidator();
void mqttMessageReceived(String &topic, String &payload);

DNSServer dnsServer;
WebServer server(80);
HTTPUpdateServer httpUpdater;
WiFiClient net;
MQTTClient mqttClient;

char mqttServerValue[STRING_LEN];
char mqttUserNameValue[STRING_LEN];
char mqttUserPasswordValue[STRING_LEN];
char mqttPublishTopicValue[STRING_LEN];

IotWebConf iotWebConf(thingName, &dnsServer, &server, wifiInitialApPassword, CONFIG_VERSION);
IotWebConfParameter mqttServerParam = IotWebConfParameter("MQTT server", "mqttServer", mqttServerValue, STRING_LEN);
IotWebConfParameter mqttUserNameParam = IotWebConfParameter("MQTT user", "mqttUser", mqttUserNameValue, STRING_LEN);
IotWebConfParameter mqttUserPasswordParam = IotWebConfParameter("MQTT password", "mqttPass", mqttUserPasswordValue, STRING_LEN, "password");
IotWebConfParameter mqttPublishTopicParam = IotWebConfParameter("MQTT Topic", "mqttTopic", mqttPublishTopicValue, STRING_LEN, "text", "DHT22_ESP8266/TempSensor");

boolean needMqttConnect = false;
boolean needReset = false;
int pinState = HIGH;
unsigned long lastReport = 0;
unsigned long lastMqttConnectionAttempt = 0;
long previousMillis = 0;

/**
 * @brief  
 * @note   
 * @retval 
 */
boolean connectMqttOptions()
{
  boolean result;
  if (mqttUserPasswordValue[0] != '\0')
  {
    result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue, mqttUserPasswordValue);
  }
  else if (mqttUserNameValue[0] != '\0')
  {
    result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue);
  }
  else
  {
    result = mqttClient.connect(iotWebConf.getThingName());
  }
  return result;
}

/**
 * @brief  Handle web requests to "/" path.
 * @note   
 * @retval None
 */
void handleRoot()
{
  // -- Let IotWebConf test and handle captive portal requests.
  if (iotWebConf.handleCaptivePortal())
  {
    // -- Captive portal request were already served.
    return;
  }
  String s = "<!DOCTYPE html><html lang=\"en\"><head>"; // redirect to config
  s += "<meta http-equiv=\"refresh\" content=\"0; URL=/config\" /></head>";
  s += "</html>\n";
  server.send(200, "text/html", s);
}

/**
 * @brief  
 * @note   
 * @retval  uint8_t
 */
uint8_t connectMqtt()
{
  unsigned long now = millis();
  /* Do not repeat within 1 sec. */
  if (1000 > now - lastMqttConnectionAttempt)
    return false;

  debugV("Connecting to MQTT server...");
  if (!connectMqttOptions())
  {
    lastMqttConnectionAttempt = now;
    return false;
  }

  String s((const __FlashStringHelper *)mqttServerValue);
  debugV("Connected to %s", s.c_str());

  mqttClient.subscribe("/test/action");
  return true;
}

/**
 * @brief  
 * @note   
 * @param  &topic: 
 * @param  &payload: 
 * @retval None
 */
void mqttMessageReceived(String &topic, String &payload)
{
  debugV("Incoming: %s - %s", topic.c_str(), payload.c_str());
}

/**
 * @brief  
 * @note   
 * @retval None
 */
void setupOTA()
{
  ArduinoOTA.setHostname(thingName); // on donne
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
    {
      type = "sketch";
    }
    else
    { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    debugD("Start updating %s", type.c_str());
  });
  ArduinoOTA.onEnd([]() {
    debugD("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    debugD("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    debugE("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR)
    {
      debugE("Auth Failed");
    }
    else if (error == OTA_BEGIN_ERROR)
    {
      debugE("Begin Failed");
    }
    else if (error == OTA_CONNECT_ERROR)
    {
      debugE("Connect Failed");
    }
    else if (error == OTA_RECEIVE_ERROR)
    {
      debugE("Receive Failed");
    }
    else if (error == OTA_END_ERROR)
    {
      debugE("End Failed");
    }
  });
  ArduinoOTA.begin();
}

/**
 * @brief  
 * @note   
 * @retval None
 */
void setup()
{
  Serial.begin(115200);

  pinMode(SENSOR_LED, OUTPUT);

  Debug.begin(thingName);
  Debug.setSerialEnabled(true); // All messages too send to serial too, and can be see in serial monitor

  debugI("Starting up...");

  timeClient.begin();

  setupOTA();

  // -- Initializing the configuration.
  iotWebConf.setStatusPin(STATUS_PIN);
  iotWebConf.setConfigPin(CONFIG_PIN);
  iotWebConf.addParameter(&mqttServerParam);
  iotWebConf.addParameter(&mqttUserNameParam);
  iotWebConf.addParameter(&mqttUserPasswordParam);
  iotWebConf.addParameter(&mqttPublishTopicParam);
  iotWebConf.setConfigSavedCallback(&configSaved);
  iotWebConf.setFormValidator(&formValidator);
  iotWebConf.setWifiConnectionCallback(&wifiConnected);
  iotWebConf.setupUpdateServer(&httpUpdater);

  boolean validConfig = iotWebConf.init();
  if (!validConfig)
  {
    mqttServerValue[0] = '\0';
    mqttUserNameValue[0] = '\0';
    mqttUserPasswordValue[0] = '\0';
    mqttPublishTopicValue[0] = '\0';
  }

  // -- Set up required URL handlers on the web server.
  server.on("/", handleRoot);
  server.on("/config", [] { iotWebConf.handleConfig(); });
  server.onNotFound([]() { iotWebConf.handleNotFound(); });

  mqttClient.begin(mqttServerValue, net);
  mqttClient.onMessage(mqttMessageReceived);

  debugI("Ready.");
}
DHTData data;
/**
 * @brief  
 * @note   
 * @retval None
 */
void loop()
{
  // -- doLoop should be called as frequently as possible.
  iotWebConf.doLoop();
  mqttClient.loop();
  // for NTP Timestamp
  timeClient.update();
  ArduinoOTA.handle();
  Debug.handle();

  if (needMqttConnect)
  {
    if (connectMqtt())
    {
      needMqttConnect = false;
    }
  }
  else if ((iotWebConf.getState() == IOTWEBCONF_STATE_ONLINE) && (!mqttClient.connected()))
  {
    debugI("MQTT reconnect");
    connectMqtt();
  }

  if (needReset)
  {
    debugI("Rebooting after 1 second.");
    iotWebConf.delay(1000);
    ESP.restart();
  }

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis > UPDATE_INTERVAL)
  {
    digitalWrite(SENSOR_LED, HIGH);
    previousMillis = currentMillis;
    data = readDHT();
    debugI("Humidity: %f%%", data.humidity);
    debugI("Temperature: %fC°", data.temperature);
    debugI("dew Point: %fC°", data.dewPoint);
  digitalWrite(SENSOR_LED, LOW);
  }
  /*unsigned long now = millis();
	if ((500 < now - lastReport) && (pinState != digitalRead(CONFIG_PIN)))
	{
		pinState = 1 - pinState; // invert pin state as it is changed
		lastReport = now;
		Serial.print("Sending on MQTT channel '/test/status' :");
		Serial.println(pinState == LOW ? "ON" : "OFF");
		
		//mqttClient.publish("/test/status", pinState == LOW ? "ON" : "OFF");
	}*/
}

/**
 * @brief  
 * @note   
 * @retval 
 */
boolean formValidator()
{
  debugV("Validating form.");
  boolean valid = true;

  int l = server.arg(mqttServerParam.getId()).length();
  if (l < 3)
  {
    mqttServerParam.errorMessage = "Please provide at least 3 characters!";
    valid = false;
  }

  return valid;
}

/**
 * @brief  
 * @note   
 * @retval None
 */
void configSaved()
{
  debugI("Configuration was updated.");
  needReset = true;
}

void wifiConnected()
{
  needMqttConnect = true;
}