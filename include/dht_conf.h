/* DHT CONF.h
 *   by Jack Lestrohan
 *
 * Created:
 *   13/02/2020 à 16:10:12
 * Last edited:
 *   13/02/2020 à 21:21:29
 * Auto updated?
 *   Yes
 *
 * Description:
 *   Header fil for dht_conf.c file
**/

#ifndef DHT_CONF_H
#define DHT_CONF_H

#include <Adafruit_Sensor.h>
#include <dht.h>

/**
 * @brief  
 * @note   
 * @retval None
 */
struct DHTData
{
  float temperature;
  float humidity;
  double dewPoint;
};

DHTData readDHT();
//float getDHTtemperature();
//float getDHTtemperatureF();
//float getDHThumidity();
//float getHeatIndex();

#endif